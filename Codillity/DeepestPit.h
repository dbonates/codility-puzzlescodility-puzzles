//
//  DeepestPit.h
//  Codillity
//
//  Created by Daniel Bonates on 6/16/15.
//  Copyright (c) 2015 Daniel Bonates. All rights reserved.
//

#ifndef Codillity_DeepestPit_h
#define Codillity_DeepestPit_h

int solution(NSMutableArray *A) {
    // write your code in Objective-C 2.0
    
    int length = (int)[A count];
    
    if (length < 2) {
        return -1;
    }
    
    int currentDepth = 0;
    int maxDepth = -1;
    
    int P, Q, R;
    int i, j, k;
    
    for (i=0; i<length-2; i++) {
        j=i+1;
        
        if (A[i] > A[j]) {
            //The biggest P.
            P = [A[i] intValue];
            
            while (j+1<length && [A[j] intValue]>[A[j+1]intValue]) {
                j++;
            }
            //The smallest Q.
            Q = [A[j] intValue];
            
            k = j+1;
            while (k+1<length && [A[k] intValue]<[A[k+1] intValue]) {
                k++;
            }
            if (k >= length) {
                break;
            }
            //The biggest R.
            R = [A[k] intValue];
            
            //printf("%d + %d + %d\n", i,j,k);
            
            currentDepth = (int)MIN(P-Q, R-Q);
            if (currentDepth > maxDepth) {
                maxDepth = currentDepth;
            }
            i = k-1;
        }
    }
    
    return maxDepth;

}

#endif

/*

 A non-empty zero-indexed array A consisting of N integers is given. A pit in this array is any triplet of integers (P, Q, R) such that:
 
 0 ≤ P < Q < R < N;
 sequence [A[P], A[P+1], ..., A[Q]] is strictly decreasing,
 i.e. A[P] > A[P+1] > ... > A[Q];
 sequence A[Q], A[Q+1], ..., A[R] is strictly increasing,
 i.e. A[Q] < A[Q+1] < ... < A[R].
 The depth of a pit (P, Q, R) is the number min{A[P] − A[Q], A[R] − A[Q]}.
 For example, consider array A consisting of 10 elements such that:
 
 A[0] =  0
 A[1] =  1
 A[2] =  3
 A[3] = -2
 A[4] =  0
 A[5] =  1
 A[6] =  0
 A[7] = -3
 A[8] =  2
 A[9] =  3
 Triplet (2, 3, 4) is one of pits in this array, because sequence [A[2], A[3]] is strictly decreasing (3 > −2) and sequence [A[3], A[4]] is strictly increasing (−2 < 0). Its depth is min{A[2] − A[3], A[4] − A[3]} = 2. Triplet (2, 3, 5) is another pit with depth 3. Triplet (5, 7, 8) is yet another pit with depth 4. There is no pit in this array deeper (i.e. having depth greater) than 4. Write a function: def solution(a) that, given a non-empty zero-indexed array A consisting of N integers, returns the depth of the deepest pit in array A. The function should return −1 if there are no pits in array A. For example, consider array A consisting of 10 elements such that:
 
 A[0] =  0
 A[1] =  1
 A[2] =  3
 A[3] = -2
 A[4] =  0
 A[5] =  1
 A[6] =  0
 A[7] = -3
 A[8] =  2
 A[9] =  3
 the function should return 4, as explained above. Assume that: N is an integer within the range [1..1,000,000]; each element of array A is an integer within the range [−100,000,000..100,000,000]. Complexity: expected worst-case time complexity is O(N); expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments). Elements of input arrays can be modified.

*/