//
//  MinAbsSliceSum.h
//  Codillity
//
//  Created by Daniel Bonates on 6/16/15.
//  Copyright (c) 2015 Daniel Bonates. All rights reserved.
//

#ifndef Codillity_MinAbsSliceSum_h
#define Codillity_MinAbsSliceSum_h

int solution(NSMutableArray *A) {
    
    
    NSArray *arr = [A sortedArrayUsingSelector:@selector(compare:)];

    
    int n = (int)[arr count];
    
    if (n < 1) {
        return 0;
    }
    if (n == 1) {
        return abs([A[0] intValue]);
    }
    
    if (n == 2) {
        return abs([A[0] intValue]+[A[1] intValue]);
    }
    
    int i = 0;
    int j = n-1;
    int minimun = INT_MAX;
    
    while (i <= j) {
        int tmp = [arr[i]intValue] + [arr[j] intValue];
        minimun = MIN(minimun, abs(tmp));
        
        if (tmp <= 0) {
            i++;
        } else {
            j--;
        }
    }
    
    return minimun;

}


#endif
