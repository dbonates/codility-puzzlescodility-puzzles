//
//  CountMultiplicativePairs.h
//  Codillity
//
//  Created by Daniel Bonates on 6/16/15.
//  Copyright (c) 2015 Daniel Bonates. All rights reserved.
//

#ifndef Codillity_CountMultiplicativePairs_h
#define Codillity_CountMultiplicativePairs_h


int solution(NSMutableArray *A, NSMutableArray *B) {
    
    if ([A count] < 1 || [A count] != [B count] || [B count] < 1 ) {
        return 0;
    }
    
    double scale = 1000000;
    NSUInteger N = [A count];
    
    
    
    int count=0;
    NSUInteger left = 0;
    NSUInteger right = N-1;
    NSUInteger C_left = 0;

    while(left<right){
        //to avoid rounding errors, use multiplication
        C_left = [A[left] doubleValue]*scale+[B[left] doubleValue];
        if(C_left>=scale){
            
            if(C_left*([A[right] doubleValue]*scale+[B[right] doubleValue])>=(C_left+([A[right] doubleValue]*scale+[B[right] doubleValue]))*scale){
                // if C[left],C[right] is ok, that means that all elements C[left++],C[right] to satisfy condition
                count+=right-left;
                right--;
            }else{
                left++;
            }
        }else{
            left++;
        }
    }
    //condition from codility.com test
    if(count>1000000000) {
        return 1000000000;
    } else {
        return count;
    }

}

int solution2(NSMutableArray *A, NSMutableArray *B) {
    
    NSUInteger lenA = [A count];
    NSUInteger lenB = [B count];
    
    if (lenA < 1 || lenB < 1 || lenA != lenB) {
        return -1;
    }
    
    NSMutableArray *tmpC = [NSMutableArray array];
    
    for (int i = 0; i < lenA; i++) {
        if ([A[i] floatValue] + [B[i] floatValue]/1000000 > 1) {
            [tmpC addObject:[NSNumber numberWithFloat:[A[i] floatValue] + [B[i] floatValue]/1000000]];
        }
    }
    
    NSArray *C = [tmpC sortedArrayUsingSelector:@selector(compare:)];
    
    long result = 0;
    
    int p = 0;
    
    NSUInteger l = [C count]-1;
    
    while (l > p) {
        long tmpResult = [C[l] longValue] / [C[l-1] longValue];
        
        while (p < l && [C[p] longValue] < tmpResult) {
            p = p+ 1;
        }
        
        if (p == l) {
            break;
        }
        
        result = result + (l-p);
        
        if (result > 1000000000) {
            return 1000000000;
        }
        
        l = l-1;

    }
    
    return (int)result;
    
}


#endif


/*

 Zero-indexed arrays A and B consisting of N non-negative integers are given. Together, they represent N real numbers, denoted as C[0], …, C[N−1]. Elements of A represent the integer parts and the corresponding elements of B (divided by 1,000,000) represent the fractional parts of the elements of C. More formally, A[I] and B[I] represent C[I] = A[I] + B[I] / 1,000,000.
 
 For example, consider the following arrays A and B:
 
 A[0] = 0  B[0] = 500,000
 A[1] = 1  B[1] = 500,000
 A[2] = 2  B[2] = 0
 A[3] = 3  B[3] = 0
 A[4] = 5  B[4] = 20,000
 They represent the following real numbers:
 
 C[0] = 0.5
 C[1] = 1.5
 C[2] = 2.0
 C[3] = 3.0
 C[4] = 5.02
 A pair of indices (P, Q) is multiplicative if 0 ≤ P < Q < N and C[P] * C[Q] ≥ C[P] + C[Q].
 
 The above arrays yield the following multiplicative pairs:
 
 (1, 3), because 1.5 * 3.0 = 4.5 ≥ 4.5 = 1.5 + 3.0,
 (1, 4), because 1.5 * 5.02 = 7.53 ≥ 6.52 = 1.5 + 5.02,
 (2, 3), because 2.0 * 3.0 = 6.0 ≥ 5.0 = 2.0 + 3.0.
 (2, 4). because 2.0 * 5.02 = 10.04 ≥ 7.02 = 2.0 + 5.02.
 (3, 4), because 3.0 * 5.02 = 15.06 ≥ 8.02 = 3.0 + 5.02.
 Write a function:
 
 class Solution { public int solution(int[] A, int[] B); }
 that, given zero-indexed arrays A and B, each containing N non-negative integers, returns the number of multiplicative pairs of indices.
 
 If the number of multiplicative pairs is greater than 1,000,000,000, the function should return 1,000,000,000.
 
 For example, given:
 
 A[0] = 0  B[0] = 500,000
 A[1] = 1  B[1] = 500,000
 A[2] = 2  B[2] = 0
 A[3] = 3  B[3] = 0
 A[4] = 5  B[4] = 20,000
 the function should return 5, as explained above.
 
 Assume that:
 
 N is an integer within the range [0..100,000];
 each element of array A is an integer within the range [0..1,000];
 each element of array B is an integer within the range [0..999,999];
 real numbers created from arrays are sorted in non-decreasing order.
 Complexity:
 
 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 
 

*/