//
//  main.m
//  Codillity
//
//  Created by Daniel Bonates on 6/15/15.
//  Copyright (c) 2015 Daniel Bonates. All rights reserved.
//

#import <Foundation/Foundation.h>

//#include "FrogJmp.h"
//#include "TapeEquilibrium.h"
//#import "PermMissingElem.h"
//#import "FrogRiverOne.h"
//#import "dboidtest.h"
//#import "PtrListLen.h"
//#import "DeepestPit.h"
//#import "MinAbsSliceSum.h"
//#import "CountMultiplicativePairs.h"
//#import "Codility.h"
#import "SegmentsCrossing.h"



int main(int argc, const char * argv[]) {
    @autoreleasepool {
    
        
        // LOGO Turttle Task
        
        NSArray *directions = @[@"North",@"East",@"South",@"West"];
        
        NSMutableArray *arr1 = [NSMutableArray arrayWithObjects: @5, @5, @1, @7, @2, @3, @5, nil]; // return 4, // collide going to west
        NSMutableArray *arr2 = [NSMutableArray arrayWithObjects: @1, @3, @2, @5, @4, @4, @6, @3, @2, nil]; //return 7 // collide going to south
        NSMutableArray *arr3 = [NSMutableArray arrayWithObjects: @2, @3, @1, @6, @2, nil]; // return 4, // collide going to west
        NSMutableArray *arr4 = [NSMutableArray arrayWithObjects: @7, @6, @6, @5, @4, @4, @2, @1, @3, @4, nil]; //return 9 // collide going to north
        NSMutableArray *arr5 = [NSMutableArray arrayWithObjects: @7, @6, @9, @5, @8, @4, @7, @3, @5, @2, @4, @1, @6, nil]; //return 13 // collide going to north
        NSMutableArray *arr6 = [NSMutableArray arrayWithObjects: @2, @1, @1, @2, nil]; //return 4 // collide going to west
        NSMutableArray *arr7 = [NSMutableArray arrayWithObjects: @4, @4, @3, @3,@2, @2, @1, @3, nil]; //return 8 // collide going to west
        NSMutableArray *arr8 = [NSMutableArray arrayWithObjects: @1, @2, @2, @1, @3, nil]; //return 5 // collide going to north
        NSMutableArray *arr9 = [NSMutableArray arrayWithObjects: @3, @1, @5, @4, @1, @5, nil]; //return 6 // collide going to east
        
        
        // just for happiness :)
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr1), directions[playground(arr1)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr2), directions[playground(arr2)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr3), directions[playground(arr3)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr4), directions[playground(arr4)%4]);
        NSLog(@"collide on step: %d \tgoing to %@\n", playground(arr5), directions[playground(arr5)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr6), directions[playground(arr6)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr7), directions[playground(arr7)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr8), directions[playground(arr8)%4]);
        NSLog(@"collide on step: %d \t\tgoing to %@\n", playground(arr9), directions[playground(arr9)%4]);
        
        
    }
    
    return 0;
}
