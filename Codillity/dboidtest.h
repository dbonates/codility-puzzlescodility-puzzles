//
//  dboidtest.h
//  Codillity
//
//  Created by Daniel Bonates on 6/16/15.
//  Copyright (c) 2015 Daniel Bonates. All rights reserved.
//

#ifndef Codillity_dboidtest_h
#define Codillity_dboidtest_h


// IntList task play around...

@interface IntList:NSObject {
    @public
        int value;
        IntList *next;
      }
 @end
@implementation IntList @end


int solution(IntList * L) {
    
    int length=0;
    while (L->next != nil){
        ++length;
    }
    
    return length;
}

#endif
