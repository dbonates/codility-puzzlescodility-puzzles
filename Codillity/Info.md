Time Complexities
https://codility.com/media/train/1-TimeComplexity.pdf


O(1) - Número fixo de operações

def constant(n):
    result = n * n
    return result



O(log n) - The value of n is halved on each iteration of the loop. If n = 2x then log n = x. How long would the program below take to execute, depending on the input data?


def logarithmic(n): result = 0
while n > 1: n //= 2
result += 1 return result


O(n) - Let’s note that if the first value of array A is 0 then the program will end immediately. But remember, when analyzing time complexity we should check for worst cases. The program will take the longest time to execute if array A does not contain any 0.

    Resumindo, o teste deve contar com uma resposta não encontrada e a operação dominante executar até o final

def linear(n, A):
    for i in xrange(n):
        if A[i] == 0: return 0
    return 1


O(n2)


O(n + m)




Space complexity
O(1) - constant numbers of variables, you also have constant space com- plexity
O(n) - If you need to declare an array with n elements, you have linear space complexity